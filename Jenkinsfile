node {
	try {
		stage('Setup') {
			checkout scm
			prepareEnv()
			initGradleProperties()
			setupCucumberProperties()
			currentBuild.description = "Setup"
			startDockerContainers()
		}
		stage('Build') {
			currentBuild.description = "Build"
		}
		stage('Test') {
			currentBuild.description = "Test"
		}
	} catch (any) {
		any.printStackTrace()
		currentBuild.result = 'FAILURE'
		throw any
	} finally {
		stopDockerContainers()
	}
}

def prepareEnv() {
	def jdkHome = tool name: 'java8', type: 'jdk'
	env.PATH = "${jdkHome}/bin:${env.PATH}"
	env.JAVA_HOME = "${jdkHome}"
	env.GRADLE_USER_HOME = "${WORKSPACE}/gradle-home"
	sh """
		mkdir -p ${env.GRADLE_USER_HOME}
		echo 'org.gradle.java.home=${env.JAVA_HOME}' > ${env.GRADLE_USER_HOME}/gradle.properties
	"""
	jdkHome = null
}

def setupCucumberProperties() {
	writeFile encoding: 'UTF-8', file: 'src/main/resources/de/abas/app/g30l0/setup.properties', text: '''EDP_CLIENT=/abas/erp
EDP_PASSWORD=sy
EDP_HOST=localhost
EDP_PORT=6560'''
}

def initGradleProperties() {
	sh './initGradleProperties.sh'
	withCredentials([usernamePassword(credentialsId: '82305355-11d8-400f-93ce-a33beb534089',
			passwordVariable: 'MAVENPASSWORD', usernameVariable: 'MAVENUSER')]) {
		sh '''
			echo nexusSnapshotURL=https://registry.abas.sh/repository/abas.snapshots/ >> gradle.properties
			echo nexusReleaseURL=https://registry.abas.sh/repository/abas.releases/ >> gradle.properties
			echo esdkSnapshotURL=https://registry.abas.sh/repository/abas.esdk.snapshots/ >> gradle.properties
			echo esdkReleaseURL=https://registry.abas.sh/repository/abas.esdk.releases/ >> gradle.properties
			echo nexusUser=$MAVENUSER >> gradle.properties
			echo nexusPassword=$MAVENPASSWORD >> gradle.properties
		'''
	}
}

def startDockerContainers() {
	withCredentials([usernamePassword(credentialsId: '82305355-11d8-400f-93ce-a33beb534089',
			passwordVariable: 'MAVENPASSWORD', usernameVariable: 'MAVENUSER')]) {
		sh 'docker login partner.registry.abas.sh -u $MAVENUSER -p $MAVENPASSWORD'
	}
	sh 'docker-compose up -d'
	sleep 30
}

def stopDockerContainers() {
	sh 'docker-compose down || true'
}
