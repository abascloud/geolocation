package de.abas.app.g30l0;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import de.abas.acceptanceTests.AbasAcceptanceRunner;
import de.abas.acceptanceTests.stepDefs.EDPOptions;


@RunWith(AbasAcceptanceRunner.class)
@EDPOptions(classpath="de/abas/app/g30l0/setup.properties")
@CucumberOptions(features = "src/integTest/resources",
		glue = { "classpath:de.abas.acceptanceTests", "classpath:my.example.project.cucumber" },
		plugin = { "de.abas.acceptanceTests.support.AbasFormatter", "pretty", "json:build/reports/cucumber.json", "html:build/reports/cucumber" },
		strict = true,
		monochrome = true)
public class RunCukes {

}