Feature: GeoLocation Infosystem

  Scenario: Create Customer
	Given I set the variable language to "EN"
	Given I open an editor "MY_EDITOR" from table "00:01" with command "STORE" for record "FISCHERFORESDK"
	And I set field "swd" to "FISCHERFORESDK"
	And I set field "addr" to "Simone Fischer"
	And I set field "street" to "Sandstrasse 55"
	And I set field "zipcode" to "39638"
	And I save the current editor
	
  Scenario: Create Vendor
    Given I set the variable language to "EN"
    Given I open an editor "MY_EDITOR" from table "01:01" with command "STORE" for record "HELLSTROEMFORESDK"
    And I set field "swd" to "HELLSTROEMFORESDK"
    And I set field "addr" to "Vanessa Hellström"
    And I set field "street" to "Sandstrasse 55"
    And I set field "zipcode" to "11552"
    And I save the current editor

  Scenario: Infosystem test
	Given I'm logged in as "sy"
	And I open the infosystem "GEOLOCATION"
	And I set field "customersel" to "FISCHERFORESDK"
	When I press start
	Then field "zipcode" has value "39638" in row !lastRow
	
  Scenario: Vendor test
    Given I'm logged in as "sy"
    And I open the infosystem "GEOLOCATION"
    And I set field "customersel" to "HELLSTROEMFORESDK"
    When I press start
    Then field "zipcode" has value "11552" in row !lastRow
	